-- -----------------------------------------------------
-- Table cliente
-- -----------------------------------------------------
CREATE TABLE cliente (
  idcliente serial,
  nombre VARCHAR(45) NOT NULL,
  apellido VARCHAR(45) NULL,
  PRIMARY KEY (idcliente));



-- -----------------------------------------------------
-- Table mesero
-- -----------------------------------------------------
CREATE TABLE mesero (
  idmesero serial,
  nombre VARCHAR(45) NULL,
  apellido VARCHAR(45) NULL,
  PRIMARY KEY (idmesero));



-- -----------------------------------------------------
-- Table menu
-- -----------------------------------------------------
CREATE TABLE menu (
  idmenu serial,
  nombre VARCHAR(45) NULL,
  valor DECIMAL NULL,
  PRIMARY KEY (idmenu));



-- -----------------------------------------------------
-- Table mesa
-- -----------------------------------------------------
CREATE TABLE mesa (
  idmesa serial,
  numero INT NULL,
  PRIMARY KEY (idmesa));


-- -----------------------------------------------------
-- Table ordencabecera
-- -----------------------------------------------------
CREATE TABLE ordencabecera (
  idordencabecera serial,
  fecha DATE NULL,
  idcliente INT NOT NULL,
  idmesero INT NOT NULL,
  idmesa INT NOT NULL,
  PRIMARY KEY (idordencabecera),
  CONSTRAINT fk_ordenCabecera_cliente
    FOREIGN KEY (idcliente)
    REFERENCES cliente (idcliente)
    ON DELETE cascade
    ON UPDATE cascade,
  CONSTRAINT fk_ordenCabecera_mesero
    FOREIGN KEY (idmesero)
    REFERENCES mesero (idmesero)
    ON DELETE cascade
    ON UPDATE cascade,
  CONSTRAINT fk_ordenCabecera_mesa
    FOREIGN KEY (idmesa)
    REFERENCES mesa (idmesa)
    ON DELETE cascade
    ON UPDATE cascade);

-- -----------------------------------------------------
-- Table ordendetalle
-- -----------------------------------------------------
CREATE TABLE ordendetalle (
  idordendetalle serial,
  idordencabecera INT NOT NULL,
  idmenu INT NOT NULL,
  cantidad INT NULL,
  PRIMARY KEY (idordendetalle),
  CONSTRAINT fk_ordendetalle_menu1
    FOREIGN KEY (idmenu)
    REFERENCES menu (idmenu)
    ON DELETE cascade
    ON UPDATE cascade,
  CONSTRAINT fk_ordendetalle_ordenCabecera1
    FOREIGN KEY (idordencabecera)
    REFERENCES ordencabecera (idordencabecera)
    ON DELETE cascade
    ON UPDATE cascade);