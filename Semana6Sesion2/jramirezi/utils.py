import psycopg2
from psycopg2 import Error as pgError

from os import system, name
from time import sleep

def Color(nomColor):
    color = {
    'white':    "\033[1;37m",
    'yellow':   "\033[1;33m",
    'green':    "\033[1;32m",
    'blue':     "\033[1;34m",
    'cyan':     "\033[1;36m",
    'red':      "\033[1;31m",
    'magenta':  "\033[1;35m",
    'black':      "\033[1;30m",
    'darkwhite':  "\033[0;37m",
    'darkyellow': "\033[0;33m",
    'darkgreen':  "\033[0;32m",
    'darkblue':   "\033[0;34m",
    'darkcyan':   "\033[0;36m",
    'darkred':    "\033[0;31m",
    'darkmagenta':"\033[0;35m",
    'darkblack':  "\033[0;30m",
    'off':        "\033[0;0m"
    }
    return color.get(nomColor)

class Conexion:
    
    def __init__(self, server='127.0.0.1', usr='postgres', psw='123456', bd='pchqtec10'):
        self.db = psycopg2.connect(
            host=server, user=usr, password=psw, database=bd)
        self.cursor = self.db.cursor()
        self.exito = False
        print("Se ha conectado correctamente a la base de datos")

    def consultarBDD(self, query):
        self.exito = False
        try:
            cur = self.cursor
            cur.execute(query)
            records = cur.fetchall()
            self.exito = True
            return records
        except Exception as error:
            print(error)
            self.exito = False
            return False

    def ejecutarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            self.db.commit()
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False
class Menu:
    def __init__(self, NombreMenu, ListaOpciones):
        self.NombreMenu = NombreMenu
        self.ListaOpciones = ListaOpciones
    
    def MostrarEncabezadoMenu(self, titulo):
        print(Color('magenta') +
                  "\t::::::::::::: Restaurante El Buen Provecho::::::::::::::"+Color('off'))
        print(Color('blue')+"\t\t:::::::::::::" + titulo + "::::::::::::::"+Color('off'))

    def MostrarMenu(self):
        self.LimpiarPantalla()
        opSalir = True
        while(opSalir):
            self.LimpiarPantalla()
            self.MostrarEncabezadoMenu(self.NombreMenu)
            for (key, value) in self.ListaOpciones.items():
                print(key, " :: ", value)
            print("Salir :: 9")
            opcion = 100
            try:
                print("Escoge tu opcion")
                opcion = int(input())
            except ValueError as error:
                print("Opcion invalida deben ser numeros de 0 - 9")
            contOpciones = 0
            for (key, value) in self.ListaOpciones.items():
                if(opcion == int(value)):
                    contOpciones += 1
                if(int(value) == 9):
                   opSalir = False 
            if(contOpciones == 0):
                print("Escoge una opcion valida")
                sleep(5)
            else:
                opSalir = False

        return opcion

    def LimpiarPantalla(self):
        def Clear():
            if name == 'nt':
            # return os.system('cls')
                return system('cls')
            else:
                # return os.system('cls')
                return system('clear')
        Clear()