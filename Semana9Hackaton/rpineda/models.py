from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from init import db
from slugify import slugify
from sqlalchemy.exc import IntegrityError
from flask import url_for


class TipoUser(db.Model):
    __tablename__ = 'farmacia_tipouser'

    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return f'<TipoUSer {self.descripcion}>'

    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return TipoUser.query.get(id)

    @staticmethod
    def get_by_descripcion(descripcion):
        return TipoUser.query.filter_by(descripcion=descripcion).first()


class User(db.Model, UserMixin):

    __tablename__ = 'farmacia_user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)
    tipouser_id = db.Column(db.Integer, db.ForeignKey('farmacia_tipouser.id', ondelete='CASCADE'), nullable=False)

    def __repr__(self):
        return f'<User {self.email}>'

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return User.query.get(id)

    @staticmethod
    def get_by_email(email):
        return User.query.filter_by(email=email).first()

class Medicamento(db.Model):
    __tablename__ = 'farmacia_medicamento'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(256), nullable=False)
    nombregenerico = db.Column(db.String(256), nullable=False)
    presentacion = db.Column(db.String(256), nullable=False)
    precio = db.Column(db.Numeric, nullable = False)
    is_igv = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'<Medicamento {self.nombre}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Medicamento.query.get(id)

    @staticmethod
    def get_by_name(nombre):
        return Medicamento.query.filter_by(nombre=nombre).first()

    @staticmethod
    def get_by_namegenerico(nombregenerico):
        return Medicamento.query.filter_by(nombregenerico=nombregenerico).first()


class Receta(db.Model):
    __tablename__ = 'farmacia_receta'

    id = db.Column(db.Integer, primary_key=True)
    medico_id = db.Column(db.Integer, db.ForeignKey('farmacia_user.id', ondelete='CASCADE'), nullable=False)
    paciente_id  = db.Column(db.Integer, db.ForeignKey('farmacia_user.id', ondelete='CASCADE'), nullable=False)
    medicamento_id = db.Column(db.Integer, db.ForeignKey('farmacia_medicamento.id', ondelete='CASCADE'), nullable=False)
    dosis = db.Column(db.String(256), unique=True, nullable=False)
    frecuencia = db.Column(db.String(256), unique=True, nullable=False)
    is_entregada = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f'<Receta {self.id}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Receta.query.get(id)

    @staticmethod
    def get_by_paciente(email):
        user = User.get_by_email(email)
        return Medicamento.query.filter_by(paciente_id=user.id).first()

    @staticmethod
    def get_by_medico(email):
        user = User.get_by_email(email)
        return Medicamento.query.filter_by(medico_id=user.id).first()
