from os import system, name
from time import sleep
import os

def Color(nomColor):
    color = {
    'white':    "\033[1;37m",
    'yellow':   "\033[1;33m",
    'green':    "\033[1;32m",
    'blue':     "\033[1;34m",
    'cyan':     "\033[1;36m",
    'red':      "\033[1;31m",
    'magenta':  "\033[1;35m",
    'black':      "\033[1;30m",
    'darkwhite':  "\033[0;37m",
    'darkyellow': "\033[0;33m",
    'darkgreen':  "\033[0;32m",
    'darkblue':   "\033[0;34m",
    'darkcyan':   "\033[0;36m",
    'darkred':    "\033[0;31m",
    'darkmagenta':"\033[0;35m",
    'darkblack':  "\033[0;30m",
    'off':        "\033[0;0m"
    }
    return color.get(nomColor)
    
class Menu:
    def __init__(self, NombreMenu, dicOpciones):
        self.NombreMenu = NombreMenu
        self.dicOpciones = dicOpciones
        
    def MostrarEncabezadoMenu(self, titulo):
        print(Color('magenta') +
                  "::::::::::::: Universidad Pachaqutec - Biblioteca Central::::::::::::::"+Color('off'))
        print(Color('blue')+"\t\t:::::::::::::" + titulo + "::::::::::::::"+Color('off'))
        
    def MostrarMenu(self):
        self.LimpiarPantalla()
        opSalir = True
        
        while(opSalir):
            
            self.LimpiarPantalla()
            self.MostrarEncabezadoMenu(self.NombreMenu)
            
            for (key, value) in self.dicOpciones.items():
                print(Color('green') + key + Color('off'), " :: ", value)
            #print("Salir :: 9")
            opcion = 100
            try:
                print(Color('darkmagenta') + "Escoge tu opcion:" + Color('off'))
                opcion = int(input())
            except ValueError as error:
                print("Opcion invalida deben ser numeros de 0 - 9")
            contOpciones = 0
            for (key, value) in self.dicOpciones.items():
                if(opcion == int(value)):
                    contOpciones += 1
            if(opcion == 8):
                opSalir = True
            if(opcion == 9):
                print("saliendo ... ")
                sleep(3)
                opSalir = False
                contOpciones = 1
                self.LimpiarPantalla() 
            if(contOpciones == 0):
                print("Escoge una opcion valida")
                sleep(5)
                          
            else:
                opSalir = False

        return opcion

    def LimpiarPantalla(self):
        def Clear():
            if name == 'nt':
            # return os.system('cls')
                return system('cls')
            else:
                # return os.system('cls')
                return system('clear')
        Clear()  


class FileManager:

    def __init__(self, nombreArchivo):
        self.nombreArchivo = nombreArchivo

    def leerArchivo(self):
        try:
            file = open(self.nombreArchivo, 'r')
            return file.read()
        except Exception as e:
            return e

    def borrarArchivo(self):
        directorioActual = os.getcwd()
        path = directorioActual+"\\"+self.nombreArchivo
        if(os.path.isfile(path)):
            try:
                os.remove(path)

            except Exception as error:
                print(error)
    def mostrarRuta(self):
        try:
            directorioActual = os.getcwd()
            return directorioActual
        except Exception as error:
            return print("Error metodo Mostrar Ruta: ",error)

    def escribirArchivo(self, linea):
        try:
            directorioActual = os.getcwd()
            path = directorioActual+"\\"+self.nombreArchivo
            if(os.path.isfile(path)):
                try:
                    # escribir el archiv
                    file = open(self.nombreArchivo, 'a')
                    file.write(linea + "\n")
                except Exception as e:
                    print(e)
                finally:
                    file.close()
            else:
                file = open(self.nombreArchivo, 'w')
                file.close()
                file = open(self.nombreArchivo, 'a')
                file.write(linea + "\n")
        except Exception as error:
            print(error)

