from django.db import models

# Create your models here.
class User(models.Model):
    nombre=models.CharField(max_length=45) 
   
    
    def __str__(self):
        return self.nombre

class Tipovehiculo(models.Model):
    descripcion=models.CharField(max_length=45) 
   
    
    def __str__(self):
        return self.descripcion

class Estado(models.Model):
    descripcion=models.CharField(max_length=45) 
   
    
    def __str__(self):
        return self.descripcion 


class Vehiculo(models.Model):
    tipovehiculo_id = models.ForeignKey(Tipovehiculo, on_delete=models.CASCADE)
    descripcion=models.CharField(max_length=45)              


    def __str__(self):
        return self.descripcion 


class User_has_vehiculo(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)        
    vehiculo_id = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)


class Posicion(models.Model):
    vehiculo_id = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)        
    vehiculo_tipovehiculo_id = models.ForeignKey(Tipovehiculo, on_delete=models.CASCADE)
    estado_id = models.ForeignKey(Estado, on_delete=models.CASCADE)
    posicion = models.CharField(max_length=50)
    fecha=models.DateField()
