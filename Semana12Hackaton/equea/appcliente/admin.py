from django.contrib import admin
from .models import User, Tipovehiculo, Estado, Vehiculo, User_has_vehiculo, Posicion
# Register your models here.

admin.site.register(User)
admin.site.register(Tipovehiculo)
admin.site.register(Estado)
admin.site.register(Vehiculo)
admin.site.register(User_has_vehiculo)
admin.site.register(Posicion)