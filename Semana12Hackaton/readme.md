- Crear un sistema de tracking de vehiculos que tenga las siguientes opciones
- - Login de usuario
- - CRUD de Vehiculos
- - Formulario de ingreso de coordenadas de vehiculo
- - Vista de Movimiento de Vehiculo

- Tomar en cuenta las siguientes consideraciones:

- - Un usuario puede tener mas de un vehiculo
- - Cada vehiculo tiene un tipo de vehiculo (camioneta, motocicleta, auto, etc.)
- - Cada punto de tracking tiene un estado (apagado, encedido, en movimiento, etc.)
- - La vista del vehiculo debe dibujar un mapa en que que se muestra el recorrido del vehiculo (opcional)

- - Ejecutar los siguientes steps iniciales:

- python -m venv env
- . env/Scripts/activate
- pip install django
- pip install mysqlclient
- django-admin startproject track .
- python manage.py startapp appcliente
- python manage.py createsuperuser
- python manage.py makemigrations appcliente
- python manage.py migrate


