class Cliente:
    def __init__(self, idcliente, nombre, apellido):
        self.idcliente = idcliente
        self.nombre = nombre
        self.apellido = apellido
    def __str__(self):
        return f"{self.idcliente}, {self.nombre}, {self.apellido}"
    def formatea(self):
        return f"\'{self.nombre}\', \'{self.apellido}\'"
    def ToDiccionary(self):
        menuDic = {self.idcliente:{
            'nombre':self.nombre,
            'apellido':self.apellido}
        }
        return menuDic
    
class Mesero:
    def __init__(self, idmesero, nombre, apellido):
        self.idmesero = idmesero
        self.nombre = nombre
        self.apellido = apellido
    def __str__(self):
        return f"{self.idmesero}, {self.nombre}, {self.apellido}"
    def formatea(self):
        return f"\'{self.nombre}\', \'{self.apellido}\'"
    def ToDiccionary(self):
        menuDic = {self.idmesero:{
            'nombre':self.nombre,
            'apellido':self.apellido}
        }
        return menuDic

class Menu:
    def __init__(self, idmenu, nombre, valor):
        self.idmenu = idmenu
        self.nombre = nombre
        self.valor = valor
    def __str__(self):
        return f"{self.idmenu}, {self.nombre}, {self.valor}"

    def ToDiccionary(self):
        menuDic = {self.idmenu: {
            'nombre':self.nombre,
            'valor':self.valor}
        }
        return menuDic

class Mesa:
    def __init__(self, idmesa, numero):
        self.idmesa = idmesa
        self.numero = numero
    def __str__(self):
        return f"idmesa: {self.idmesa}, numero de Mesa: {self.numero}"

class Orden:
    def __init__(self, idorden, fecha, idcliente, idmesero, idmesa):
        self.idorden = idorden
        self.fecha = fecha
        self.idcliente = idcliente
        self.idmesero = idmesero
        self.idmesa = idmesa

class OrdenDetalle:
    def __init__(self,idordendetalle, idorden, idmenu, cantidad):
        self.idordendetalle = idordendetalle
        self.idorden = idorden
        self.idmenu = idmenu
        self.cantidad = cantidad