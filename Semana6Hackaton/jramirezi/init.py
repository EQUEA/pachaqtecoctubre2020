from clientesCRUD import ClienteList
from meserosCRUD import MeseroList
from menuCRUD import MenuList
from mesasCRUD import MesaList
import utils
from utils import Color, Menu
from time import sleep
from modelos import Cliente
from time import sleep
#Se inicializan las listas:
lstCliente = lstMesero = lstMenu = lstMesa = lstOrden = lstOrdenDetalle = lstClienteEnMesa = []
query = ''
list_dic_ordemenu = []
orden_cliente = {}
#Se crean los objetos:
clienteList = ClienteList(lstCliente)
meseroList = MeseroList(lstMesero)
menuList = MenuList(lstMenu)
mesaList = MesaList(lstMesa)
#Creamos el método que carga los datos a las listas desde la base de datos:
def CargaInicial():
    clienteList.CargarData()
    meseroList.CargarData()
    menuList.CargarData()
    mesaList.CargarData()

#Creamos la estructua del menú:

CargaInicial()

opciones = {"Realizar Pedido" : 1, "Mantenedor": 2}
menuPrincipal = utils.Menu("Menu Principal",opciones)
result = menuPrincipal.MostrarMenu()
if result == 1: ######## Opcion Realizar Pedido #############
    print("estoy en Realizar pedido")
    opciones = {"Tomar Pedido":1, "Pagar Pedido": 2}
    menuOrden = utils.Menu(" # Menu de Orden de Compra #", opciones)
    result = menuOrden.MostrarMenu()
    
    if result == 1: ####### menu Tomar Pedido #####
        ##### Aqui buscamos el id del Cliente, para luego llenar el orden cabecera:
        busqueda = clienteList.Buscar()        
        if( busqueda == False):
            print(utils.Color('red')+"Se tiene que registrar el nuevo Cliente: "+ utils.Color('off'))
            nombre = input("\tEscribe el Nombre del Nuevo Cliente: ")
            apellido = input("\tEscribe el Apellido del Cliente: ")
            client = (nombre, apellido)
            id_Cliente = clienteList.Agregar(client)
            clienteEnMesa = Cliente(id_Cliente, nombre, apellido)
            lstClienteEnMesa.append(clienteEnMesa)
        if(busqueda == True):
            id_Cliente = input("\n\tIngresar el Id del Cliente para realizar la orden: ")
            clienteEnMesa = clienteList.BuscarPorID(id_Cliente)
            lstClienteEnMesa.append(clienteEnMesa)
        ##### Aqui buscamos el id del Mesero, para luego llenar el orden cabecera:
        menuOrden.LimpiarPantalla()
        menuOrden.MostrarEncabezadoMenu("\tToma de Orden: Listado de Meseros")
        meseroList.Imprimir()
        id_Mesero = input("\n\tIngresar el Id del Mesero para realizar la orden: ")
        ##### Aqui buscamos el id del Mesa, para luego llenar el orden cabecera:
        menuOrden.LimpiarPantalla()
        menuOrden.MostrarEncabezadoMenu("\tToma de Orden: Listado de Mesas")
        mesaList.Imprimir()
        id_Mesa = input("\n\tIngresar el ID de la Mesa para realizar la orden: ")
        print(clienteEnMesa,"Id Cliente: ",id_Cliente, "Id mesero: ",id_Mesero, "Id Mesa", id_Mesa)
        ##### Aqui buscamos el id del Plato, para luego llenar el orden detalle:
        #list_dic_ordemenu = []
        try:
            while True:
                menuOrden.LimpiarPantalla()
                menuOrden.MostrarEncabezadoMenu("\tToma de Orden: Ingreso de Pedido")
                opc1 = int(input(utils.Color('red')+"\n\tEscribe (1) para tomar orden o escribe (9) para salir: "+utils.Color('off')))
                if(opc1 == 9):
                    break
                menuList.Buscar()
                
                codPlato = int(input("Ingrese el codigo del Plato:" ))
                for itemObjmMenu in menuList.Get():
                    dicc_menu = itemObjmMenu.ToDiccionary()
                    
                    if(itemObjmMenu.idmenu == codPlato):
                        print("bingo!", dicc_menu[itemObjmMenu.idmenu])
                        list_dic_ordemenu.append(dicc_menu)
                sleep(3)
        except Exception as err:
            print(err)
        orden_cliente[id_Mesa]=list_dic_ordemenu
        ###### Haciendo el Insert ####
        print(list_dic_ordemenu)
    if result == 2: ####### menu Pagar Pedido #####
        pass
if result == 2: ######## Opcion Mantenedor #############
    opciones = {"Clientes":1, "Mesero": 2, "Menu": 3, "Mesas": 4 }
    menuMantenedor = utils.Menu("Menu de Mantenedores", opciones)
    result = menuMantenedor.MostrarMenu()
    if result == 1: ####### menu clientes ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuCliente = utils.Menu("Menu de Clientes", opciones)
        result = menuCliente.MostrarMenu()
        if result == 1: ####### Cliente - Crear #####
            nombre = input("Escribe el Nombre del Cliente: ")
            apellido = input("Escribe el Apellido del Cliente: ")
            client = (nombre, apellido)
            clienteList.Agregar(client)
        if result ==2:  ####### Cliente - Buscar #####
            clienteList.Buscar()
            sleep(6)
        if result == 3: ####### Cliente - Actualizar  #####
            clienteList.Buscar()
            clienteList.Actualizar()
        if result == 4: ####### Cliente - Borrar  #####
            clienteList.Buscar()
            clienteList.Borrar()
    if result == 2: ####### menu mesero ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMesero = utils.Menu("Menu Mesero", opciones)
        result = menuMesero.MostrarMenu()
        
        if result == 1: ####### Mesero - Crear #####
            nombre = input("Escribe el Nombre del Mesero: ")
            apellido = input("Escribe el Apellido del Mesero: ")
            mozo = (nombre, apellido)
            meseroList.Agregar(mozo)
        if result ==2:  ####### Mesero - Buscar #####
            meseroList.Buscar()
            sleep(6)     
        if result == 3: ####### Mesero - Actualizar  #####
            meseroList.Buscar()
            meseroList.Actualizar()       
        if result ==4: ####### Mesero - Borrar  #####
            meseroList.Buscar()
            meseroList.Actualizar()
    if result == 3: ####### Menu de Restaurant ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMenu = utils.Menu("Menu ", opciones)
        result = menuMenu.MostrarMenu()
        
        if result == 1: ####### Crear Plato del Menu #####
            nombre = input("Escribe el Nombre del plato a solicitar: ")
            valor = int(input("Escribe el precio del plato: "))
            plato = (nombre, valor)
            menuList.Agregar(plato)
        if result ==2:  ####### Menu - Buscar #####
            menuList.Buscar()
            sleep(6)
        if result == 3: ####### Menu- Actualizar  #####
            menuList.Buscar()
            menuList.Actualizar()
        if result ==4: ####### Menu - Borrar  #####
            menuList.Buscar()
            menuList.Borrar()
    if result == 4: ####### Menu Mesas ##### 
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuMesa = utils.Menu("Mesas ", opciones)
        result = menuMesa.MostrarMenu()
        
        if result == 1: ####### Crear #####
            numero = int(input("Escribe el numero de mesa : "))
            mesaList.Agregar(numero)
        if result ==2:  ####### Mesa - Buscar #####
            mesaList.Buscar()
            sleep(6)         
        if result == 3: ####### Mesa - Actualizar  #####
           mesaList.Buscar()
           mesaList.Actualizar()             
        if result ==4: ####### Mesa - Borrar  #####
           mesaList.Buscar()
           mesaList.Borrar()
        