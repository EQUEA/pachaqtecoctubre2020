from utils import Conexion
import utils
from models import Cliente, Mesero, Menu, Mesa, Orden, OrdenDetalle
import sys

lstCliente = lstMesero = lstMenu = lstMesa = lstOrden = lstOrdenDetalle = []
query = ''


def CargaInicial():
    conn = Conexion()
    query = 'Select * from cliente'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Cliente(objResult[0], objResult[1], objResult[2])
        lstCliente.append(objClass)

    query = 'Select * from mesero'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Mesero(objResult[0], objResult[1], objResult[2])
        lstMesero.append(objClass)

    query = 'Select * from menu'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Menu(objResult[0], objResult[1], objResult[2])
        lstMenu.append(objClass)

    query = 'Select * from mesa'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Mesa(objResult[0], objResult[1])
        lstMesa.append(objClass)

    query = 'Select * from ordencabecera'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Orden(objResult[0], objResult[1],
                         objResult[2], objResult[3], objResult[4])
        lstOrden.append(objClass)

    query = 'Select * from ordendetalle'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = OrdenDetalle(
            objResult[0], objResult[1], objResult[2], objResult[2])
        lstOrdenDetalle.append(objClass)


CargaInicial()

opciones = {"Realizar Pedido": 1, "Mantenedor": 2}
menuPrincipal = utils.Menu("Menu Principal", opciones)
result = menuPrincipal.MostrarMenu()


if result == 2:
    opciones = {"Clientes": 1, "Mesero": 2, "Menu": 3, "Mesas": 4}
    menuMantenedor = utils.Menu("Menu de Mantenedores", opciones)
    result = menuMantenedor.MostrarMenu()
    if result == 1:
        opciones = {"Crear": 1, "Buscar": 2, "Actualizar": 3, "Borrar": 4}
        menuCliente = utils.Menu("Menu de Clientes", opciones)
        result = menuCliente.MostrarMenu()
        if result == 1:
            nombre = input("Escribe el Nombre del Cliente: ")
            apellido = input("Escribe el Apellido del Cliente: ")
            query = f"insert into cliente (nombre, apellido ) values('{nombre}','{apellido}')"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Cliente Creado Correctamente")
                lstCliente = []
                CargaInicial()
        if result == 2:
            nombre = input("Escribe el Nombre del Cliente a buscar: ")
            query = f"select * from cliente where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        if result == 3:
            nombre = input("Escribe el Nombre del Cliente a buscar: ")
            query = f"select * from cliente where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idCliente = int(
                input("escoje el id del cliente que deseas Actualizar"))
            nombre = input("Escribe el Nombre del Cliente: ")
            apellido = input("Escribe el Apellido del Cliente: ")
            query = f"update cliente set nombre = '{nombre}', apellido = '{apellido}' where idcliente = {idCliente}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Cliente Actualizado Correctamente")
                lstCliente = []
                CargaInicial()
        if result == 4:
            nombre = input("Escribe el Nombre del Cliente a buscar: ")
            query = f"select * from cliente where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idCliente = int(
                input("escoje el id del cliente que deseas Borrar "))
            query = f"delete from cliente where idcliente = {idCliente}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Cliente Borrado Correctamente")
                lstCliente = []
                CargaInicial()
    if result == 2:
        opciones = {"Crear": 1, "Buscar": 2, "Actualizar": 3, "Borrar": 4}
        menuMesero = utils.Menu("Menu de Mesero", opciones)
        result = menuMesero.MostrarMenu()
        if result == 1:
            nombre = input("Escribe el Nombre del Mesero: ")
            apellido = input("Escribe el Apellido del Mesero: ")
            query = f"insert into mesero (nombre, apellido ) values('{nombre}','{apellido}')"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Creado Correctamente")
                lstMesero = []
                CargaInicial()
        if result == 2:
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            query = f"select * from mesero where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        if result == 3:
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            query = f"select * from mesero where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idMesero = int(
                input("escoje el id del Mesero que deseas Actualizar"))
            nombre = input("Escribe el Nombre del Mesero: ")
            apellido = input("Escribe el Apellido del Mesero: ")
            query = f"update mesero set nombre = '{nombre}', apellido = '{apellido}' where idmesero = {idMesero}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Actualizado Correctamente")
                lstMesero = []
                CargaInicial()
        if result == 4:
            nombre = input("Escribe el Nombre del Mesero a buscar: ")
            query = f"select * from mesero where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idMesero = int(input("escoje el id del Mesero que deseas Borrar "))
            query = f"delete from mesero where idmesero = {idMesero}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesero Borrado Correctamente")
                lstMesero = []
                CargaInicial()
    if result == 3:
        opciones = {"Crear": 1, "Buscar": 2, "Actualizar": 3, "Borrar": 4}
        menuMenu = utils.Menu("Menu de Menu", opciones)
        result = menuMenu.MostrarMenu()
        if result == 1:
            nombre = input("Escribe el Nombre del Menu: ")
            valor = input("Escribe el Valor del Menu: ")
            query = f"insert into menu (nombre, valor ) values('{nombre}','{valor}')"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Creado Correctamente")
                lstMenu = []
                CargaInicial()
        if result == 2:
            nombre = input("Escribe el Nombre del Menu a buscar: ")
            query = f"select * from menu where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        if result == 3:
            nombre = input("Escribe el Nombre del Menu a buscar: ")
            query = f"select * from menu where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idMenu = int(input("escoje el id del Menu que deseas Actualizar"))
            nombre = input("Escribe el Nombre del Menu: ")
            valor = input("Escribe el Valor del Menu: ")
            query = f"update menu set nombre = '{nombre}', valor = '{valor}' where idmenu = {idMenu}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Actualizado Correctamente")
                lstMenu = []
                CargaInicial()
        if result == 4:
            nombre = input("Escribe el Nombre del Menu a buscar: ")
            query = f"select * from menu where nombre like '%{nombre}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idMenu = int(input("escoje el id del Menu que deseas Borrar "))
            query = f"delete from menu where idmenu = {idMenu}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Menu Borrado Correctamente")
                lstMenu = []
                CargaInicial()
    if result == 4:
        opciones = {"Crear": 1, "Buscar": 2, "Actualizar": 3, "Borrar": 4}
        menuMesa = utils.Menu("Menu de Mesa", opciones)
        result = menuMesa.MostrarMenu()
        if result == 1:
            numero = input("Escribe el Numero de la Mesa: ")
            query = f"insert into mesa (numero) values('{numero}')"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Creado Correctamente")
                lstMesa = []
                CargaInicial()
        if result == 2:
            numero = input("Escribe el Numero de la Mesa a buscar: ")
            query = f"select * from mesa where nombre like '%{numero}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]} ")

        if result == 3:
            numero = input("Escribe el Numero de la Mesa a buscar: ")
            query = f"select * from mesa where nombre like '%{numero}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]} ")
            idMesa = int(
                input("escoje el id de la Mesa que deseas Actualizar"))
            numero = input("Escribe el Numero de la Mesa: ")
            query = f"update mesa set numero = '{numero}' where idmesa = {idMesa}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Actualizado Correctamente")
                lstMesa = []
                CargaInicial()
        if result == 4:
            numero = input("Escribe el Numero de la Mesa a buscar: ")
            query = f"select * from mesa where nombre like '%{numero}%';"
            conn = Conexion()
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]} ")
            idMesa = int(input("escoje el id del Mesa que deseas Borrar "))
            query = f"delete from mesa where idmesa = {idMesa}"
            conn = Conexion()
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Mesa Borrado Correctamente")
                lstMesa = []
                CargaInicial()
        if result == 9:
           sys.exit()
            
if result == 1:
    opciones = {"idorden": 1, "fecha": 2,
                "idcliente": 3, "idmesero": 4, "idmesa": 5}
    menuOrden = utils.Menu("Menu de Orden", opciones)
    result = menuOrden.MostrarMenu()
    fecha = input("escribe la Fecha: ")
    idcliente = int(input("Escribe el IdCliente: "))
    idmesero = int(input("Escribe el IdMesero: "))
    idmesa = int(input("Escribe el IdMesa: "))
    query = f"insert into ordencabecera (fecha, idcliente, idmesero, idmesa) values ('{fecha}', '{idcliente}', '{idmesero}' , '{idmesa}') "
    conn = Conexion()
    connResult = conn.ejecutarBDD(query)
    if(connResult):
        print("Ordencabecera Actualizado Correctamente")
        lstOrden = []
        CargaInicial()

    idorden = input("escribe el IdOrden: ")
    idcliente = int(input("Escribe el IdCliente: "))
    idmenu = int(input("Escribe el IdMenu: "))
    cantidad = int(input("Escribe la Cantidad: "))
    query = f"insert into ordendetalle (idorden, idcliente, idmenu, cantidad) values ('{idorden}', '{idcliente}', '{idmenu}' , '{cantidad}') "
    conn = Conexion()
    connResult = conn.ejecutarBDD(query)
    if(connResult):
        print("OrdenDetalle '{idorden}', '{idcliente}', '{idmenu}' , '{cantidad}'")
        lstOrdenDetalle = []
        CargaInicial()    
if result == 9:
    sys.exit()
