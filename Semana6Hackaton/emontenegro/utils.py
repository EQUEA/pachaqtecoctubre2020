import psycopg2
from psycopg2 import Error as pgError

import os
from time import sleep

class Conexion:
    def __init__(self, server='127.0.0.1', port='15432', usr='postgres', psw='p0stgr3s', bd='pchqtc10'):
        self.db = psycopg2.connect(
            host=server, port=port, user=usr, password=psw, database=bd)
        self.cursor = self.db.cursor()
        print("Se ha conectado correctamente a la base de datos")

    def consultarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            records = cur.fetchall()
            return records
        except Exception as error:
            print(error)
            return False

    def ejecutarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            self.db.commit()
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False
class Menu:
    def __init__(self, NombreMenu, ListaOpciones):
        self.NombreMenu = NombreMenu
        self.ListaOpciones = ListaOpciones

    def MostrarMenu(self):
        self.LimpiarPantalla()
        opSalir = True
        while(opSalir):
            self.LimpiarPantalla()
            # Mostramos el Menu
            print(f"""\033[1;34m
::::::::::::: EMPRESA MONTENEGRO ::::::::::::::
::::::::::::: {self.NombreMenu} ::::::::::::::
                  \033[0;m""")
            for (key, value) in self.ListaOpciones.items():
                print(value, "->", key)
            print("9 -> Salir")
            opcion = 100

            # Toma de Valores
            try:
                opcion = int(input("Escoge tu opcion: "))
            except ValueError as error:
                print("Opcion invalida deben ser numeros de 0 - 9")

            # Proceso
            contOpciones = 0
            for (key, value) in self.ListaOpciones.items():
                if(opcion == int(value)):
                    contOpciones += 1
            if(contOpciones == 0):
                print("Escoge una opcion valida")
                sleep(5)
            else:
                opSalir = False

        return opcion

    def LimpiarPantalla(self):
        def Clear():
            # return os.system('cls')
            return os.system('clear')
        Clear()