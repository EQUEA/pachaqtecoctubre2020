class Pinturas:
    def __init__(self, color, codigo, marca, referencia, tipo):
        self.color = color
        self.codigo = codigo
        self.marca = marca
        self.referencia = referencia
        self.tipo = tipo
    
    def toDict(self):
        dicPintura = {
            "color": self.color,
            "codigo": self.codigo,
            "marca": self.marca,
            "referencia": self.referencia,
            "tipo": self.tipo
        }
        return dicPintura

class Pinceles:
    def __init__(self, codigo, marca):
        self.codigo = codigo
        self.marca = marca

class Tipo:
    def __init__(self, nombre):
        self.nombre = nombre

class Marca:
    def __init__(self, nombre):
        self.nombre = nombre

