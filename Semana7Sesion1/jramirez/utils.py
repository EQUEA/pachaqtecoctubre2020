from pymongo import MongoClient, errors

import os 
from time import sleep

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    CEND = '\033[0m'

class Conexion:
    def __init__(self, uri, database):
        self.client = MongoClient(uri)
        self.db = self.client[database]
        print('Conexión a la base de datos, exitosa')
    
    def insertar_registro(self, collection, data):
        try:
            collection = self.db[collection]
            result = collection.insert_one(data)
            print(f'Inserted row: {result.inserted_id}')
            return True
        except Exception as error:
            print(error)
            return False
    
    def insertar_registros(self,collection, data):
        collection = self.db[collection]
        result = collection.insert_many(data)
        print(f'Inserted rows: {result.inserted_ids}')        

    def obtener_registros(self, collection, condition={}):
        try:
            collection = self.db[collection]
            data = collection.find(condition)
            return list(data)
        except Exception as error:
            print(error)
            sleep(5)
            return False
        
    def obtener_registro(self, collection, condition={}):
        collection = self.db[collection]
        data = collection.find_one(condition)
        return list(data)

    def actualizar_registro(self, collection, condition, change):
        collection = self.db[collection]
        collection.update_one(condition, {
            '$set': change
        })
        print(f'Se actualizo un registro')

    def eliminar_registro(self, collection, condition):
        collection = self.db[collection]
        collection.delete_one(condition)
        print(f'Se elimino un registro')
        
    def cerrar_conexion(self):
        self.db.close()
        print('Se cerro la conexión con exito !')
        return True

class Menu:
    def __init__(self, nombreMenu, listaOpciones):
        self.nombreMenu = nombreMenu
        self.listaOpciones = listaOpciones

    def mostrarMenu(self):
        self.limpiarPantalla()
        opSalir = True
        while(opSalir):
            self.limpiarPantalla()
            print(color.BLUE+":::::::::::::BIENVENIDOS EMPRESA ESMR::::::::::::::"+color.CEND)
            print(color.BLUE+":::::::::::::::::::" +self.nombreMenu + "::::::::::::::::::"+color.CEND)
            
            for (key, value) in self.listaOpciones.items():
                print(key, "\t:: ", value)
            opcion = 100
            print("\t- Salir \t\t::  9")
            try:
                print(color.CYAN+"Escoge tu opcion"+color.CEND)
                opcion = int(input())
            except ValueError as error:
                self.__log.error(error)
                print(color.RED+"Opcion invalida deben ser numeros del 0 al 2"+color.CEND)
            contOpciones = 0
            for (key, value) in self.listaOpciones.items():
                if(opcion == int(value) or opcion == 9):
                   contOpciones += 1
            if(contOpciones == 0):
                print(color.RED+"Escoge una opcion valida"+color.CEND)
                self.__log.debug("No escoje opion")
                sleep(3)
            else:
                opSalir = False

        return opcion

    def limpiarPantalla(self):
        def clear():
            #return os.system('cls')
            return os.system('clear')
        clear()


# try:
#     #conectado desde python a mi mongo
#     cliente = MongoClient('mongodb://localhost:27017')
#     db = cliente.pachaqtec
#     print(cliente)
#     print(db)
#     collection = db.posts
#     myPost = {
#         'Titulo':'Mi primer Post',
#         'Contenido':'Hola este es mi primer post',
#         'Autor' : "Mery Molina"
#     }
#     result = collection.insert_one(myPost)
#     arrPosts = []
    
#     for obj in range(1,10,1):
#         myPost = {
#             'Titulo':'Mi primer Post : ' + str(obj),
#             'Contenido':'Hola este es mi primer post :' +str(obj),
#             'Autor' : "Mery Molina" 
#         }
#         arrPosts.append(myPost)
#     result = collection.insert_many(arrPosts)

#     print(f"El id del post ingresado es {result.inserted_id}")
# except errors.ConfigurationError as error:
#     print(error)
# except Exception as error:
#     print(error)
# finally:
#     if cliente:
#         cliente.close()
