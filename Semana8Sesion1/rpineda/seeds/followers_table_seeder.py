from orator.seeds import Seeder


class FollowersTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        with self.db.transaction():
            karen = self.db.table('users').where('name', 'Karen').first()
            roberto = self.db.table('users').where('name', 'Roberto').first()
            david = self.db.table('users').where('name', 'David').first()

            self.db.table('followers').insert([
                {'follower_id': karen.id, 'followed_id': roberto.id},
                {'follower_id': roberto.id, 'followed_id': david.id},
                {'follower_id': david.id, 'followed_id': karen.id},
            ])

