from orator.seeds import Seeder


class UsersTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('users').insert({
            'name': 'Karen',
            'email': 'karen@x-codec.net'
        })
        self.db.table('users').insert({
            'name': 'Roberto',
            'email': 'rpineda@x-codec.net'
        })
        self.db.table('users').insert({
            'name': 'David',
            'email': 'dlopez@x-codec.net'
        })

