from utils import conexionBDD
import utils
from modelos import Usuario, Sala, Reunion

lstUsuario = lstSala = lstReunion = []
query = ''

def CargaInicial():
    conn = conexionBDD(2)
    query = 'Select * from Usuario'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Usuario(objResult[0],objResult[1],objResult[2],objResult[3],objResult[4] )
        lstUsuario.append(objClass)
    
    query = 'Select * from sala'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Sala(objResult[0],objResult[1],objResult[2])
        lstSala.append(objClass)
    
    query = 'Select * from Reunion'
    result = conn.consultarBDD(query)
    for objResult in result:
        objClass = Reunion(objResult[0],objResult[1] )
        lstReunion.append(objClass)

CargaInicial()

opciones = {"Mantenedor" : 1, "Iniciar Sesion": 2}
menuPrincipal = utils.Menu("Menu Principal",opciones)
result = menuPrincipal.mostrarMenu()

if result == 1:
    opciones = {"Usuarios":1, "Salas": 2 }
    menuMantenedor = utils.Menu("Menu de Mantenedores", opciones)
    result = menuMantenedor.mostrarMenu()

    if result == 1:
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuUsuario = utils.Menu("Menu de Usuarios", opciones)
        result = menuUsuario.mostrarMenu()

        if result == 1:
            username = input("Escribe el Nombre de Usuario: ")
            email = input("Escribe el Email del Usuario: ")
            password = input("Escribe el Password del usuario: ")
            query = f"insert into Usuario (username, email, password) values('{username}','{email}','{password}')"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Usuario Creado Correctamente")
                lstUsuario = []
                CargaInicial()

        if result == 2:
            username = input("Escribe el Nombre de Usuario a buscar: ")
            query = f"select * from Usuario where username like '%{username}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}, {objResult[3]}, {objResult[4]}")

        if result == 3:
            username = input("Escribe el Nombre de Usuario a buscar: ")
            query = f"select * from Usuario where username like '%{username}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}, {objResult[3]}, {objResult[4]}")
            idUser = int(input("escoje el id del Usuario que deseas Actualizar"))
            username = input("Escribe el Nombre de Usuario: ")
            email = input("Escribe el Email del Usuario: ")
            password = input("Escribe el Password del usuario: ")
            query = f"update Usuario set username = '{username}', email = '{email}', password = '{password}' where iduser = {idUser}"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Usuario Actualizado Correctamente")
                lstUsuario = []
                CargaInicial()

        if result == 4:
            username = input("Escribe el Nombre de Usuario a buscar: ")
            query = f"select * from Usuario where username like '%{username}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}, {objResult[3]}, {objResult[4]}")
            idUser = int(input("escoje el id del Usuario que deseas Borrar "))
            query = f"delete from cliente where iduser = {idUser}"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Usuario Borrado Correctamente")
                lstUsuario = []
                CargaInicial()


    if result == 2:
        opciones = {"Crear":1, "Buscar": 2, "Actualizar": 3, "Borrar": 4 }
        menuSala = utils.Menu("Menu de Salas", opciones)
        result = menuSala.mostrarMenu()

        if result == 1:
            nombreSala = input("Escribe el Nombre de Sala: ")
            query = f"insert into sala (nombresala) values('{nombreSala}')"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Sala Creada Correctamente")
                lstSala = []
                CargaInicial()

        if result == 2:
            nombreSala = input("Escribe el Nombre de Sala a buscar: ")
            query = f"select * from sala where nombresala like '%{nombreSala}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        if result == 3:
            nombreSala = input("Escribe el Nombre de Sala a buscar: ")
            query = f"select * from sala where nombresala like '%{nombreSala}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idSala = int(input("escoje el id de la Sala que deseas Actualizar"))
            nombreSala = input("Escribe el Nombre de Sala: ")
            query = f"update sala set nombresala = '{nombreSala}' where idsala = {idSala}"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Sala Actualizada Correctamente")
                lstSala = []
                CargaInicial()

        if result == 4:
            nombreSala = input("Escribe el Nombre de Sala a buscar: ")
            query = f"select * from sala where nombresala like '%{nombreSala}%';"
            conn  = conexionBDD(2)
            connResult = conn.consultarBDD(query)
            for objResult in connResult:
                print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")
            idSala = int(input("escoje el id de la Sala que deseas Borrar "))
            query = f"delete from sala where idsala = {idSala}"
            conn  = conexionBDD(2)
            connResult = conn.ejecutarBDD(query)
            if(connResult):
                print("Sala Borrada Correctamente")
                lstSala = []
                CargaInicial()

if result == 2:
    print(f"=== INICIAR SESION ===")
    username = input("Ingrese el Usuario: ")
    password = input("Ingrese la Contraseña: ")

    query = f"select * from Usuario where username='{username}' and password='{password}';"
    conn  = conexionBDD(2)
    connResult = conn.consultarBDD(query)

    if int(len(connResult)) > 0:
        userId = connResult[0][0]
        print(f"==== Bienvenido {username} ====")
        print(f"+++ Lista de Salas Activas +++")

        query = f"select * from sala"
        conn  = conexionBDD(2)
        connResult = conn.consultarBDD(query)
        for objResult in connResult:
            print(f"{objResult[0]}, {objResult[1]}, {objResult[2]}")

        idSala = int(input("Digite el Id de la Sala a Ingresar: "))

        query = f"insert into reunion (iduser, idsala) values('{userId}','{idSala}')"
        conn  = conexionBDD(2)
        connResult = conn.ejecutarBDD(query)
        if(connResult):
            print(f"Has Ingresado a la Sala {idSala}")

