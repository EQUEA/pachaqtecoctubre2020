#➔ Crea una lista llamada personas cuyo contenido sera de los nombres Jorge, Juan
# Carlos y 3 numeros cuyos valores son 4,5,6
personas = ["Jorge", "Juan", "Carlos", 4, 5, 6]
print(personas)
 #➔ Ahora añade un nuevo elemento a la lista con el valor “Dexter”
personas.append("Dexter")
print(personas)
# ➔ Ahora obtén el índice del elemento cuyo valor sea Juan
print(personas.index("Juan"))

# ➔ Obtén el índice del valor 5
print(personas.index(5))
# ➔ Finalmente elimina el elemento cuyo valor es Carlos
personas.remove("Carlos")
print(personas)