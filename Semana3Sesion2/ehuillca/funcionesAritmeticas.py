'''
Este modulo esta para definir las operaciones aritmeticas
'''


def Suma(numero1, numero2):
    return numero1 + numero2

def Resta(numero1, numero2):
    return numero1 - numero2

def Multiplicacion(numero1, numero2):
    return numero1 * numero2

def Division(numero1, numero2):
    if(numero2==0):
        return "No se pude dividir por 0"

    return numero1 / numero2

def Potencia(numero1, numero2):
    return numero1 ** numero2
