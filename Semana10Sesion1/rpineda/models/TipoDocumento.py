from Database import db


class TipoDocumento(db.Model):
    __tablename__ = 'farmacia_tipodocumento'

    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return f'<TipoDocumento {self.descripcion}>'

    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return TipoDocumento.query.get(id)

    @staticmethod
    def get_by_descripcion(descripcion):
        return TipoDocumento.query.filter_by(descripcion=descripcion).first()
