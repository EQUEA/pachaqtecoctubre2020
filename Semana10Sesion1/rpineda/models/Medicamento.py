from Database import db


class Medicamento(db.Model):
    __tablename__ = 'farmacia_medicamento'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(256), nullable=False)
    nombregenerico = db.Column(db.String(256), nullable=False)
    presentacion = db.Column(db.String(256), nullable=False)
    precio = db.Column(db.Numeric(10,2), nullable = False)
    is_igv = db.Column(db.Boolean, default=False)


    def __repr__(self):
        return f'<Medicamento {self.nombre}>'
    
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_id(id):
        return Medicamento.query.get(id)

    @staticmethod
    def get_by_name(nombre):
        return Medicamento.query.filter_by(nombre=nombre).first()

    @staticmethod
    def get_by_namegenerico(nombregenerico):
        return Medicamento.query.filter_by(nombregenerico=nombregenerico).first()
