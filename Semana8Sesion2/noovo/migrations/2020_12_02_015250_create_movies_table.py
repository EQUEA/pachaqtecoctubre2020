from orator.migrations import Migration


class CreateMoviesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('movies') as table:
            table.string('source')
            table.increments('id')
            table.string('title')
            table.string('thumbnail')
            table.string('summary')
            table.string('video')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('movies')
