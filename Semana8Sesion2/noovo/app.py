import os
from flask import Flask, request
from flask_orator import Orator, jsonify
from orator.orm import belongs_to, has_many, belongs_to_many, Collection

import json
# Configuration
DEBUG = True
ORATOR_DATABASES = {
    'default': 'postgres',
    'postgres': {
        'driver': 'postgres',
        'host': 'localhost',
        'database': 'video',
        'user': 'postgres',
        'password': '123456',
        'prefix': ''
    }
}
# ORATOR_DATABASES = {
#     'default': 'mysql',
#     'mysql': {
#         'driver': 'mysql',
#         'host': 'localhost',
#         'database': 'chat',
#         'user': 'root',
#         'password': 'pachaqtec',
#         'prefix': ''
#     }
# }

# Creating Flask application
app = Flask(__name__)
app.config.from_object(__name__)

# Initializing Orator
db = Orator(app)

class Movie(db.Model):
    __fillable__ = ['source', 'title', 'thumbnail', 'summary', 'video']

@app.route('/', methods=['GET'])
def home():
    return "Hola desde Flask ORATOR"

@app.route('/vod/list/simple/Education', methods=['GET'])
def get_all_vodEducation():
    movie = Movie.all()
    
    return jsonify(movie) 
    
def method_name():
   pass
if __name__ == '__main__':
    app.run(debug=True,host="192.168.0.5", port=40000)
    
