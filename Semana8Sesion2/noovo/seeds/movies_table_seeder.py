from orator.seeds import Seeder


class MoviesTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        with self.db.transaction():
            self.db.table('movies').insert([
    {
        "source": "noovo",
        "title": "Peru Education Vod Test1",
        "thumbnail": "https://elgatogringo.com/imagen/APRENDO_EN_CASA_3RO_DE_SECUNDARIA_COMUNICACION_LUNES_09_NOVIEMBRE_TV_PERU_cover.jpg",
        "summary": "Peru Education Vod Test1",
        "video": "https://elgatogringo.com/imagen/video.mp4"
    },
    {
        "source": "noovo",
        "title": "Aprendo En Casa 10-11-2020 1 Y 2 De Primaria",
        "thumbnail": "https://elgatogringo.com/imagen/APRENDO_EN_CASA_2_DE_SECUNDARIA_COMUNICACI%C3%93N_LUNES_09_DE_NOVIEMBRE_EN_VIVO_TV_PERU_cover.jpg",
        "summary": "Primaria 1.° y 2.° grado. Ciencia y tecnología/Comunicación. Tema: ¡Seguimos experimentando! Descubrimos cómo la masa influye en nuestro experimento.",
        "video": "https://elgatogringo.com/imagen/video.mp4"
    },
    {
        "source": "mstore",
        "title": "Aprendo en casa 051120",
        "thumbnail": "https://elgatogringo.com/imagen/APRENDO_EN_CASA%20_4TO_DE_SECUNDARIA_COMUNICACION_LUNES_09_NOVIEMBRE_TV_PERU_cover.jpg",
        "summary": "Video de aprendizaje Minedu",
        "video": "https://elgatogringo.com/imagen/video.mp4"
    },
    {
        "source": "mstore",
        "title": "Aprendo en casa 09112020",
        "thumbnail": "https://elgatogringo.com/imagen/APRENDO_EN_CASA_3RO_DE_SECUNDARIA_COMUNICACION_LUNES_09_NOVIEMBRE_TV_PERU_cover.jpg",
        "summary": "Video de aprendizaje Minedu",
        "video": "https://elgatogringo.com/imagen/video.mp4"
    },
            ])
