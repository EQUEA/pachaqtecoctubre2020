#Pedir Valor Unitario
#Multiplicar el valor unitario por la cantidad
#Evaluar cúanto vamos a pagar de impuesto a la renta (4%)
try:
    valorUnitario = int(input("Ingresa el valor unitario:"))
    cantidad = int(input("ingresa la cantidad:"))
    baseImponible = 5000
    impuesto = 0
    porcentanjeImpuesto = 0.05
    subtotal = valorUnitario*cantidad
    if(subtotal >= baseImponible):
        impuesto = subtotal*porcentanjeImpuesto
        total = impuesto + subtotal
    else:
        total = subtotal
    print(f"El total a pagar es: {total}, tu impuesto es: {impuesto}")
except Exception as error:
    print(f"ha ocurrido un esrroerror, el error es: {error}")