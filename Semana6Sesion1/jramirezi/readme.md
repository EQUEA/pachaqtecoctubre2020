### Creacion de entorno virtual:
python -m venv mientorno
. mientorno/Scripts/activate

### Comandos PIP en mi entono:

```sh
pip list
// Instalamos el conector de base de datos:
pip install psycopg2
```

### Conectado a una base de datos postgrest: Puerto Postgres: 5432

```sh
import psycopg2

conn = psycopg2.connect(user='postgres', password ='123456', host ='127.0.0.1', database = 'modelos')

cursor = conn.cursor()
cursor.execute("select version();")
result = cursor.fetchall()
print(result)
```
