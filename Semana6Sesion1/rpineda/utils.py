import psycopg2
from psycopg2 import Error as pgError

class Conexion:
    def __init__(self, server='127.0.0.1', usr='postgres', psw='pachaqtec', bd='pachaqtec'):
        self.db = psycopg2.connect(
            host=server, user=usr, password=psw, database=bd)
        self.cursor = self.db.cursor()
        print("Se ha conectado correctamente a la base de datos")

    def consultarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            records = cur.fetchall()
            return records
        except Exception as error:
            print(error)
            return False

    def ejecutarBDD(self, query):
        try:
            cur = self.cursor
            cur.execute(query)
            self.db.commit()
            exito = True
            return exito
        except Exception as identifier:
            print(identifier)
            return False

