'''
Inicio del Programa
'''
import problemas
import os
def clear():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

clear()
menu = True

while menu:
    
    try:
        prob = int(input("""
        ###################################################
        Escoja un problema (1 - 15) o digita 0 para salir: """))
        print("\n")
        if(prob == 0):
            print("Saliendo del programa")
            menu = False
        if(prob == 1):
            peso = int(input("Digita tu peso en numeros "))
            edad = int(input("Digita tu edad en numeros "))
            respuesta = problemas.problema1(edad, peso)
            print(f"la respuesta es {respuesta}")
        if(prob == 15):
            text = problemas.problema15()       
            print(text)
    except Exception as error:
        print(f"Ha ocurrido un error: {error}")
    
