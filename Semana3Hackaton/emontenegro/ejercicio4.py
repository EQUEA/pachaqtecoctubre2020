'''
Escribir un método que dados 2 valores de entrada imprima siempre la división del mayor entre el menor.
'''

# Import math Library
import math

while True:
    try:
        valor1 = float(input("Digitar el valor 1: "))
        valor2 = float(input("Digitar el valor 2: "))
        
        resultado = valor1 / valor2 if valor1 > valor2 else valor2 / valor1
        print(f"La division del mayor sobre el menor es: {resultado}")
        break
    except ValueError:
        print(f"Ingresar valores validos (Numeros)")
    except Exception as error:
        print(f"Se ha encontrado un error: {error}")


        