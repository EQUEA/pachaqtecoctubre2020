CREATE SCHEMA odavila


CREATE TABLE Alumnos (
  idAlumno Serial NOT NULL, 
  Nombre VARCHAR(45) NOT NULL,
  Apellido_paterno VARCHAR(45) NOT NULL,
  Apellido_materno VARCHAR(45) NOT NULL,
  DNI VARCHAR(45) NOT NULL,
  Correo VARCHAR(45) NOT NULL,
  Edad INT NOT NULL,
  PRIMARY KEY (idAlumno))


  CREATE TABLE  Salon (
  idSalon Serial NOT NULL ,
  Nombre VARCHAR(45) NOT NULL,
  Año_escolar VARCHAR(45) NOT NULL,
  PRIMARY KEY (idSalon))


CREATE TABLE Curso (
  idCurso Serial NOT NULL ,
  Nombre_Curso VARCHAR(45) NOT NULL,
  Profesor VARCHAR(45) NOT NULL)
  
  CREATE TABLE Profesor (
  idProfesor  Serial NOT NULL,
  Nombre VARCHAR(45) NOT NULL,
  Apellido_Paterno VARCHAR(45) NOT NULL,
  Apellido_Materno VARCHAR(45) NOT NULL,
  DNI VARCHAR(45) NOT NULL,
  Edad Serial NOT NULL,
  Correo VARCHAR(45) NOT NULL)