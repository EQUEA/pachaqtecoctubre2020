CREATE SCHEMA rpineda;

CREATE TABLE Alumno (
  idAlumno Serial NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  apellido_paterno VARCHAR(45) NOT NULL,
  apellido_materno VARCHAR(45) NULL,
  dni VARCHAR(45) NOT NULL,
  edad INT NOT NULL,
  correo VARCHAR(45) NOT NULL,
  PRIMARY KEY (idAlumno))