INSERT INTO Salon (nombre) VALUES
("1roA"),
("1roB"),
("2doA"),
("2doB"),
("3roA"),
("3roB"),
("4toA"),
("4toB"),
("5toA"),
("5toB");

INSERT INTO Alumno (nombre, edad, correo, IdSalon) VALUES 
("Bruno Montenegro", 24, "brumontenegro@gmail.com", 1),
("Alcides Montenegro", 28, "amontenegro@gmail.com", 3),
("Estephanie Montenegro", 26, "esmontenegro@gmail.com", 5),
("Elmer Montenegro", 35, "elmontenegro@gmail.com", 1),
("Brian Montenegro", 32, "brimontenegro@gmail.com", 5);

INSERT INTO Profesor (nombre, edad, correo, IdSalon) VALUES 
("Yuri Montenegro", 42, "yurimontenegro@gmail.com", 1),
("Jaime Montenegro", 39, "jaimemontenegro@gmail.com", 3) ,
("Luis Montenegro", 41, "luismontenegro@gmail.com", 5);

INSERT INTO Curso (nombre) VALUES 
("Matematica"),
("Aritmetica"),
("Geometria"),
("Trigonometria");

INSERT INTO ProfesorCurso (IdProfesor, IdCurso) VALUES 
(1, 3),
(1, 4),
(2, 1),
(3, 1),
(3, 2),
(3, 3),
(3, 4);

INSERT INTO AnioEscolar (descripcion) VALUES 
("2019"),
("2020");

INSERT INTO Bimestre (descripcion) VALUES 
("1er"),
("2do"),
("3er"),
("4to");

INSERT INTO AlumnoAnioEscolarBimetre (IdAlumno, IdAnioEscolar, IdBimestre, nota) VALUES
(1, 1, 1, 14), 
(1, 1, 2, 13), 
(1, 1, 3, 12), 
(1, 1, 4, 16), 
(2, 1, 1, 14), 
(2, 1, 2, 13), 
(2, 1, 3, 8), 
(2, 1, 4, 16),
(3, 1, 1, 14), 
(3, 1, 2, 13), 
(3, 1, 3, 11), 
(3, 1, 4, 20),
(1, 2, 1, 14), 
(1, 2, 2, 13), 
(1, 2, 3, 18),  
(2, 2, 1, 3), 
(2, 2, 2, 13), 
(2, 2, 3, 11), 
(3, 2, 1, 14), 
(3, 2, 2, 4), 
(3, 2, 3, 11), 
(4, 2, 1, 14), 
(4, 2, 2, 19), 
(4, 2, 3, 11), 
(5, 2, 1, 8), 
(5, 2, 2, 13), 
(5, 2, 3, 11); 

INSERT INTO SalonProfesorCurso (IdSalon, IdProfesor, IdCurso) VALUES
(1, 1, 1), 
(3, 1, 1), 
(5, 2, 2), 
(7, 2, 2), 
(9, 3, 3), 
(1, 3, 3), 
(2, 1, 3), 
(4, 1, 4), 
(6, 2, 4), 
(8, 3, 4), 
(10, 3, 4);