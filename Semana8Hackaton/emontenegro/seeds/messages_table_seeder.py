from orator.seeds import Seeder


class MessagesTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        meet1 = self.db.table('meets').where('id', 1).first()
        meet2 = self.db.table('meets').where('id', 2).first()
        meet3 = self.db.table('meets').where('id', 3).first()

        self.db.table('messages').insert({
            'content': 'Karen\'s message.',
            'meet_id': meet1['id']
        })

        self.db.table('messages').insert({
            'content': 'Roberto\'s message.',
            'meet_id': meet2['id']
        })

        self.db.table('messages').insert({
            'content': 'David\'s message.',
            'meet_id': meet3['id']
        })

