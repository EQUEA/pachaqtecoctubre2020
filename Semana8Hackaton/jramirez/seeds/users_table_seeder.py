from orator.seeds import Seeder


class UsersTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.db.table('users').insert({
            'username': 'latina',
            'email': 'mery.molina@elgatogringo.com',
            'password':'123456'
        })
        self.db.table('users').insert({
            'username': 'roberto',
            'email': 'rpineda@x-codec.net',
            'password' : '123456'
        })
        self.db.table('users').insert({
            'username': 'julio',
            'email': 'julio@codigowebsite.com',
            'password' : '123456'
        })
        self.db.table('users').insert({
            'username': 'jorge',
            'email': 'jorge@codigowebsite.com',
            'password' : '123456'
        })
        self.db.table('users').insert({
            'username': 'gringo',
            'email': 'gringo@elgatogringo.com',
            'password' : '123456'
        })

