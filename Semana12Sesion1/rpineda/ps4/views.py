from django.shortcuts import render
from django.views import generic
from .models import Game


# Create your views here.

class IndexView(generic.ListView):
    template_name = 'ps4/index.html'
    context_object_name = 'latest_game_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Game.objects.order_by('-estreno')[:5]
